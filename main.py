#Remeber install Flask

# import main Flask class and request object
from flask import Flask, request

# create the Flask app
app = Flask(__name__)

@app.route('/query', methods=["GET"])
def query_example():
    language = request.args.get('language')
    return '''<h1>The language value is: {}</h1>'''.format(language),200

if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(port=5000)
